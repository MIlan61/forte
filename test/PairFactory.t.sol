pragma solidity ^0.8.16;
import { PairFactory } from "src/factories/PairFactory.sol";
import { Test } from "forge-std/Test.sol";

contract TestPairFactory is Test {

    function testChangeFeeRecipient() public {
        PairFactory fac = new PairFactory();
        address priorRecipient = fac.feeRecipient();
        address newRecipient = address(0xb33f);
        fac.setFeeRecipient(newRecipient);
        assertEq(fac.feeRecipient(), priorRecipient); // shouldn't change until accepted
        vm.prank(newRecipient);
        fac.acceptFeeRecipient();
        assertEq(fac.feeRecipient(), newRecipient);

    }

}