pragma solidity ^0.8.16;

import './BaseTest.sol';

contract PairTest is BaseTest {
    function deployPairCoins() public {
        deployOwners();
        deployCoins();
        mintStables();
    }

    function testConfirmUsdcDeployment() public {
        deployPairCoins();
        assertEq(USDC.name(), "USDC");
    }

    function confirmFraxDeployment() public {
        testConfirmUsdcDeployment();

        assertEq(FRAX.name(), "FRAX");
    }

    function confirmTokensForFraxUsdc() public {
        confirmFraxDeployment();
        deployPairFactoryAndRouter();
        deployPairWithOwner(address(owner));
        deployPairWithOwner(address(owner2));

        (address token0, address token1) = router.sortTokens(address(USDC), address(FRAX));
        assertEq(pair.token0(), token0);
        assertEq(pair.token1(), token1);
    }

    function mintAndBurnTokensForPairFraxUsdc() public {
        confirmTokensForFraxUsdc();

        USDC.transfer(address(pair), USDC_1);
        FRAX.transfer(address(pair), TOKEN_1);
        pair.mint(address(owner));
        assertEq(pair.getAmountOut(USDC_1, address(USDC)), 982117769725505988); // Stableswap pool, so we get tight slippage
        (uint256 amount, bool stable) = router.getAmountOut(USDC_1, address(USDC), address(FRAX));
        assertEq(pair.getAmountOut(USDC_1, address(USDC)), amount);
        assertTrue(stable);
        assertTrue(router.isPair(address(pair)));
    }

    function mintAndBurnTokensForPairFraxUsdcOwner2() public {
        mintAndBurnTokensForPairFraxUsdc();

        owner2.transfer(address(USDC), address(pair), USDC_1);
        owner2.transfer(address(FRAX), address(pair), TOKEN_1);
        owner2.mint(address(pair), address(owner2));
        assertEq(owner2.getAmountOut(address(pair), USDC_1, address(USDC)), 992220948146798746); // Tighter slippage after adding more liq
    }

    function routerAddLiquidity() public {
        mintAndBurnTokensForPairFraxUsdcOwner2();

        USDC.approve(address(router), USDC_100K);
        FRAX.approve(address(router), TOKEN_100K);
        router.addLiquidity(address(FRAX), address(USDC), true, TOKEN_100K, USDC_100K, TOKEN_100K, USDC_100K, address(owner), block.timestamp);
        USDC.approve(address(router), USDC_100K);
        FRAX.approve(address(router), TOKEN_100K);
        router.addLiquidity(address(FRAX), address(USDC), false, TOKEN_100K, USDC_100K, TOKEN_100K, USDC_100K, address(owner), block.timestamp);
        DAI.approve(address(router), TOKEN_100M);
        FRAX.approve(address(router), TOKEN_100M);
        router.addLiquidity(address(FRAX), address(DAI), true, TOKEN_100M, TOKEN_100M, 0, 0, address(owner), block.timestamp);
    }

    function routerRemoveLiquidity() public {
        routerAddLiquidity();

        USDC.approve(address(router), USDC_100K);
        FRAX.approve(address(router), TOKEN_100K);
        router.quoteAddLiquidity(address(FRAX), address(USDC), true, TOKEN_100K, USDC_100K);
        router.quoteRemoveLiquidity(address(FRAX), address(USDC), true, USDC_100K);
    }

    function routerAddLiquidityOwner2() public {
        routerRemoveLiquidity();

        owner2.approve(address(USDC), address(router), USDC_100K);
        owner2.approve(address(FRAX), address(router), TOKEN_100K);
        owner2.addLiquidity(payable(address(router)), address(FRAX), address(USDC), true, TOKEN_100K, USDC_100K, TOKEN_100K, USDC_100K, address(owner2), block.timestamp);
        owner2.approve(address(USDC), address(router), USDC_100K);
        owner2.approve(address(FRAX), address(router), TOKEN_100K);
        owner2.addLiquidity(payable(address(router)), address(FRAX), address(USDC), false, TOKEN_100K, USDC_100K, TOKEN_100K, USDC_100K, address(owner2), block.timestamp);
        owner2.approve(address(DAI), address(router), TOKEN_100M);
        owner2.approve(address(FRAX), address(router), TOKEN_100M);
        owner2.addLiquidity(payable(address(router)), address(FRAX), address(DAI), true, TOKEN_100M, TOKEN_100M, 0, 0, address(owner2), block.timestamp);
    }

    function routerPair1GetAmountsOutAndSwapExactTokensForTokens() public {
        routerAddLiquidityOwner2();

        Router.route[] memory routes = new Router.route[](1);
        routes[0] = Router.route(address(USDC), address(FRAX), true);

        assertEq(router.getAmountsOut(USDC_1, routes)[1], pair.getAmountOut(USDC_1, address(USDC)));

        uint256[] memory assertedOutput = router.getAmountsOut(USDC_1, routes);
        USDC.approve(address(router), USDC_1);
        router.swapExactTokensForTokens(USDC_1, assertedOutput[1], routes, address(owner), block.timestamp);
        vm.warp(block.timestamp + 1801);
        vm.roll(block.number + 1);
        address fees = pair.fees();
        assertEq(USDC.balanceOf(fees), 100);
        uint256 b = USDC.balanceOf(address(owner));
        pair.claimFees();
        assertEq(USDC.balanceOf(address(owner)), b + 41); // (5/6) * 100 / 2 == 41.6666667
        uint256 b2 = USDC.balanceOf(address(owner2));
        owner2.claimFees(address(pair));
        assertEq(USDC.balanceOf(address(owner2)), b2 + 41); // (5/6) * 100 / 2 == 41.6666667
    }

    function routerPair1GetAmountsOutAndSwapExactTokensForTokensOwner2() public {
        routerPair1GetAmountsOutAndSwapExactTokensForTokens();

        Router.route[] memory routes = new Router.route[](1);
        routes[0] = Router.route(address(USDC), address(FRAX), true);

        assertEq(router.getAmountsOut(USDC_1, routes)[1], pair.getAmountOut(USDC_1, address(USDC)));

        uint256[] memory expectedOutput = router.getAmountsOut(USDC_1, routes);
        owner2.approve(address(USDC), address(router), USDC_1);
        owner2.swapExactTokensForTokens(payable(address(router)), USDC_1, expectedOutput[1], routes, address(owner2), block.timestamp);
        address fees = pair.fees();
        assertEq(USDC.balanceOf(fees), 118); // 200 - 41 * 2 = 118
        uint256 b = USDC.balanceOf(address(owner));
        owner2.claimFees(address(pair));
        assertEq(USDC.balanceOf(address(owner)), b);

        uint256 prior_balance = USDC.balanceOf(factory.feeRecipient());
        pair.claimProtocolFees();
        assertEq(USDC.balanceOf(factory.feeRecipient()), prior_balance + 32); // 200 in total fees / 6 ~= 33, TODO: double check fp loss 
    }

    function routerPair2GetAmountsOutAndSwapExactTokensForTokens() public {
        routerPair1GetAmountsOutAndSwapExactTokensForTokensOwner2();

        Router.route[] memory routes = new Router.route[](1);
        routes[0] = Router.route(address(USDC), address(FRAX), false);

        assertEq(router.getAmountsOut(USDC_1, routes)[1], pair2.getAmountOut(USDC_1, address(USDC)));

        uint256[] memory expectedOutput = router.getAmountsOut(USDC_1, routes);
        USDC.approve(address(router), USDC_1);
        router.swapExactTokensForTokens(USDC_1, expectedOutput[1], routes, address(owner), block.timestamp);
    }

    function routerPair3GetAmountsOutAndSwapExactTokensForTokens() public {
        routerPair2GetAmountsOutAndSwapExactTokensForTokens();

        Router.route[] memory routes = new Router.route[](1);
        routes[0] = Router.route(address(FRAX), address(DAI), true);

        assertEq(router.getAmountsOut(TOKEN_1M, routes)[1], pair3.getAmountOut(TOKEN_1M, address(FRAX)));

        uint256[] memory expectedOutput = router.getAmountsOut(TOKEN_1M, routes);
        FRAX.approve(address(router), TOKEN_1M);
        router.swapExactTokensForTokens(TOKEN_1M, expectedOutput[1], routes, address(owner), block.timestamp);
    }

    function routerPair1GetAmountsOutAndSwapExactTokensForTokens2() public {
        routerPair3GetAmountsOutAndSwapExactTokensForTokens();

        Router.route[] memory routes = new Router.route[](1);
        routes[0] = Router.route(address(USDC), address(FRAX), true);

        uint256[] memory expectedOutput = router.getAmountsOut(USDC_1, routes);
        USDC.approve(address(router), USDC_1);
        router.swapExactTokensForTokens(USDC_1, expectedOutput[1], routes, address(owner), block.timestamp);
    }

    function routerPair2GetAmountsOutAndSwapExactTokensForTokens2() public {
        routerPair1GetAmountsOutAndSwapExactTokensForTokens2();

        Router.route[] memory routes = new Router.route[](1);
        routes[0] = Router.route(address(USDC), address(FRAX), false);

        uint256[] memory expectedOutput = router.getAmountsOut(USDC_1, routes);
        USDC.approve(address(router), USDC_1);
        router.swapExactTokensForTokens(USDC_1, expectedOutput[1], routes, address(owner), block.timestamp);
    }

    function routerPair1GetAmountsOutAndSwapExactTokensForTokens2Again() public {
        routerPair2GetAmountsOutAndSwapExactTokensForTokens2();

        Router.route[] memory routes = new Router.route[](1);
        routes[0] = Router.route(address(FRAX), address(USDC), false);

        uint256[] memory expectedOutput = router.getAmountsOut(TOKEN_1, routes);
        FRAX.approve(address(router), TOKEN_1);
        router.swapExactTokensForTokens(TOKEN_1, expectedOutput[1], routes, address(owner), block.timestamp);
    }

    function routerPair2GetAmountsOutAndSwapExactTokensForTokens2Again() public {
        routerPair1GetAmountsOutAndSwapExactTokensForTokens2Again();

        Router.route[] memory routes = new Router.route[](1);
        routes[0] = Router.route(address(FRAX), address(USDC), false);

        uint256[] memory expectedOutput = router.getAmountsOut(TOKEN_1, routes);
        FRAX.approve(address(router), TOKEN_1);
        router.swapExactTokensForTokens(TOKEN_1, expectedOutput[1], routes, address(owner), block.timestamp);
    }

    function routerPair1Pair2GetAmountsOutAndSwapExactTokensForTokens() public {
        routerPair2GetAmountsOutAndSwapExactTokensForTokens2Again();

        Router.route[] memory route = new Router.route[](2);
        route[0] = Router.route(address(FRAX), address(USDC), false);
        route[1] = Router.route(address(USDC), address(FRAX), true);

        uint256 before = FRAX.balanceOf(address(owner)) - TOKEN_1;

        uint256[] memory expectedOutput = router.getAmountsOut(TOKEN_1, route);
        FRAX.approve(address(router), TOKEN_1);
        router.swapExactTokensForTokens(TOKEN_1, expectedOutput[2], route, address(owner), block.timestamp);
        uint256 after_ = FRAX.balanceOf(address(owner));
        assertEq(after_ - before, expectedOutput[2]);
    }

    function routerAddLiquidityOwner3() public {
        routerPair1Pair2GetAmountsOutAndSwapExactTokensForTokens();

        owner3.approve(address(USDC), address(router), 1e12);
        owner3.approve(address(FRAX), address(router), TOKEN_1M);
        owner3.addLiquidity(payable(address(router)), address(FRAX), address(USDC), true, TOKEN_1M, 1e12, 0, 0, address(owner3), block.timestamp);
    }

    function test() public {
        routerAddLiquidityOwner3();
    }
}