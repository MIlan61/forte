pragma solidity ^0.8.16;

import { MockERC20 } from "solmate/test/utils/mocks/MockERC20.sol";
import { Pair } from "src/Pair.sol";
import { PairFactory } from "src/factories/PairFactory.sol";
import { Router } from "src/Router.sol";

contract TestOwner {
    /*//////////////////////////////////////////////////////////////
                               MockERC20
    //////////////////////////////////////////////////////////////*/

    function approve(address _token, address _spender, uint256 _amount) public {
        MockERC20(_token).approve(_spender, _amount);
    }

    function transfer(address _token, address _to, uint256 _amount) public {
        MockERC20(_token).transfer(_to, _amount);
    }

    /*//////////////////////////////////////////////////////////////
                                  Pair
    //////////////////////////////////////////////////////////////*/

    function claimFees(address _pair) public {
        Pair(_pair).claimFees();
    }

    function mint(address _pair, address _to) public {
        Pair(_pair).mint(_to);
    }

    function getAmountOut(address _pair, uint256 _amountIn, address _tokenIn) public view returns (uint256) {
        return Pair(_pair).getAmountOut(_amountIn, _tokenIn);
    }

    /*//////////////////////////////////////////////////////////////
                               PairFactory
    //////////////////////////////////////////////////////////////*/

    function setFeeManager(address _factory, address _feeManager) public {
        PairFactory(_factory).setFeeManager(_feeManager);
    }

    function acceptFeeManager(address _factory) public {
        PairFactory(_factory).acceptFeeManager();
    }

    function setFee(address _factory, bool _stable, uint256 _fee) public {
        PairFactory(_factory).setFee(_stable, _fee);
    }

    /*//////////////////////////////////////////////////////////////
                                Router
    //////////////////////////////////////////////////////////////*/

    function addLiquidity(address payable _router, address _tokenA, address _tokenB, bool _stable, uint256 _amountADesired, uint256 _amountBDesired, uint256 _amountAMin, uint256 _amountBMin, address _to, uint256 _deadline) public {
        Router(_router).addLiquidity(_tokenA, _tokenB, _stable, _amountADesired, _amountBDesired, _amountAMin, _amountBMin, _to, _deadline);
    }
    
    function swapExactTokensForTokens(address payable _router, uint256 _amountIn, uint256 _amountOutMin, Router.route[] calldata _routes, address _to, uint256 _deadline) public {
        Router(_router).swapExactTokensForTokens(_amountIn, _amountOutMin, _routes, _to, _deadline);
    }
}