// SPDX-License-Identifier: MIT
pragma solidity ^0.8.16;

import "forge-std/Script.sol";
import { PairFactory } from "../src/factories/PairFactory.sol";
import { Router } from "../src/Router.sol";
import { IWCANTO } from "../src/interfaces/IWCANTO.sol";
import { IERC20 } from "../src/interfaces/IERC20.sol";

contract MyScript is Script {

    IWCANTO wcanto = IWCANTO(0x826551890Dc65655a0Aceca109aB11AbDbD7a07B);
    IERC20 wcantoErc20 = IERC20(0x826551890Dc65655a0Aceca109aB11AbDbD7a07B);
    IERC20 usdc = IERC20(0x80b5a32E4F032B2a058b4F29EC95EEfEEB87aDcd);

    function run() external {
        // PairFactory factory = new PairFactory();
        // Router router = new Router(address(factory), address(wcanto));

        PairFactory factory = PairFactory(0x759e390D946249c63e0A1d8a810C5A577a591719);
        Router router = Router(payable(0x543dB6c8137c74952dA1830b6E502912608c0E69));
        vm.label(address(factory), "PairFactory");
        vm.label(address(router), "Router");
        vm.label(address(wcanto), "WCANTO");
        vm.label(address(usdc), "USDC");

        vm.startBroadcast();

        usdc.approve(address(router), type(uint256).max);
        wcantoErc20.approve(address(router), type(uint256).max);
        address(wcanto).call{value: 200e18}(abi.encodeWithSelector(0xd0e30db0));
        router.addLiquidity(
            address(usdc),
            address(wcanto),
            false,
            100e6,
            200e18,
            0,
            0,
            msg.sender,
            type(uint256).max
        );

        // router.addLiquidityCANTO{value: 200e18}(
        //     address(usdc),
        //     false,
        //     100e6,
        //     1,
        //     200e18,
        //     msg.sender,
        //     type(uint256).max
        // );

        vm.stopBroadcast();
    }
}